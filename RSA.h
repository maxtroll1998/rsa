#pragma once
#include <iostream>
#include <iomanip>
#include <math.h>  
using namespace std;

uint64_t msb_mul(uint64_t a, uint64_t b);

class bit2048
{
	public:
	
		bit2048(uint64_t data_i[32]) {for(int i = 0; i< 32; i++) data[i] = data_i[i];}
		bit2048() {for(int i = 0; i< 32; i++) data[i] = 0;}
		
		uint64_t data_get(int i) const {return data[i];}
		void data_set(int i,uint64_t d) {data[i] = d;}
		
		friend ostream& operator<<(ostream &f, const bit2048 &n){
			bool started = 0;
			for(int i = 0; i< 32; i++){
				if(!started){
					if(n.data[i]!=0 || i == 31){
						f<<hex<<setfill('0')<<n.data[i];
						started = 1;
						}
					}
				else f<<hex<<setfill('0')<<setw(16)<<n.data[i];
			}
			return f;
		}
		
		
		bit2048 operator+(const bit2048 &n) const {
			bool c = 0;
			bit2048 s;
			for(int i = 31; i>= 0; i--)
			{
				s.data_set(i,data[i] + n.data_get(i) + c);
				if((data[i]>>1) + (n.data_get(i)>>1) >= 0x8000000000000000) c = 1;
				else if((data[i]>>1) + (n.data_get(i)>>1) == 0x0FFFFFFFFFFFFFFF && data[i]<<63 == 0x8000000000000000 && n.data_get(i)<<63 == 0x8000000000000000) c = 1;
				else if(data[i]+ n.data_get(i) == 0xFFFFFFFFFFFFFFFF && c == 1) c = 1;
				else c = 0;
				
			}
			return s;
		}
		
		bit2048& operator+=(const bit2048 &n) {
			*this = *this+n;
			return *this;
		}
		
		bit2048 operator-(const bit2048 &n) const {
			bool c = 0;
			bit2048 d;
			for(int i = 31; i>= 0; i--)
			{
				d.data_set(i,data[i] - n.data_get(i) - c);
				if(data[i] < n.data_get(i)) c = 1;
				else if(data[i] == n.data_get(i) && c == 1) c=1;
				else c = 0;
				
			}
			return d;
		}
		
		bit2048& operator-=(const bit2048 &n) {
			*this = *this-n;
			return *this;
		}
		
		bit2048 operator*(const bit2048 &n) const {
			uint64_t p0[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
			bit2048 p(p0);
			bit2048 pp(p0);
			bit2048 c(p0);
			for(int j = 31; j> 15; j--)
			{
				for(int i = 31; i> 15; i--)
				{
					pp.data_set(i-31+j,data[i]*n.data_get(j));
					if(i-31+j>0) c.data_set(i-32+j,msb_mul(data[i],n.data_get(j)));
				}
				p+=pp;
				p+=c;
				pp = bit2048(p0);
				c = bit2048(p0);
			}
			return p;
		}
		
		bit2048 operator>>(const int &n) const {
			uint64_t r0[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
			bit2048 r(r0);
			int n1 = n%64;
			r.data_set(n/64, data[0]>>n1);
			for(int i = 1; i < 32-n/64; i++)
			{
				r.data_set(i+n/64, (data[i]>>n1) + (data[i-1]<<(64-n1)));
			}
			return r;
		}
		
	private:
	
		uint64_t data[32];
};

bit2048 montgomery(bit2048 A, bit2048 B, bit2048 N);