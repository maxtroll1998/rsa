# Rapport d’implémentation chifffrement RSA

**Intro et résultats**

Le code rendu est incomplet mais contient toutes les structures et fonctions de base nécessaires à la réalisation du chiffrement RSA. 
Le temps total de chiffrement n’a donc pas pu être mesuré, mais a titre indicatif le code renvoie le temps d’exécution des opérations utilisées pour le debug. On a ainsi entre une 20aine et une 30aine de ms pour l’exécution de 3 multiplications, 3 additions/soustractions et un décalage de bits. (Par hasard, ces opérations correspondent relativement bien à l’algorithme de Montgomery, on pourrait donc estimer a une 30aine de secondes le temps d’une multiplication modulaire).

**Structure choisie**

Ce projet a été réalisé en C++, pour bénéficier des performances d’un langage de ce niveau, tout en utilisant les classes qui n’existent pas en C. Tout le projet repose donc sur une classe bit2048 correspondant à la structure de grand nombre demandée, et la définition de ses différents opérateurs.
La structure de données choisie est celle qui est la plus optimale en terme de mémoire pour l’architecture de la machine : un tableau de 32 blocs de 64bits ( uint64_t data[32] ). La particularité est que le tableau est parcouru dans le sens croissant par le constructeur et l’affichage, et donc dans le sens décroissant par les différents opérateurs.

**Opérations implémentées**

Les opérations implémentées sont celles qui servent à l’implémentation de l’algorithme de multiplication modulaire de Montgomery ainsi qu’au debug :
-	Constructeur à partir d’une liste d’uint64_t
-	Affichage d’un bit2048
-	Addition (addition par bloc avec propagation de la carry)
-	Soustraction (soustraction par bloc avec propagation de la carry)
-	+= et -= 
-	Multiplication (nécessite une fonction qui renvoie les bits de poids fort d’une multiplication 64, réalisée sous forme d’une somme de produits et carry partiels)
-	Décalage de bits vers la droite (pour la division par R)

