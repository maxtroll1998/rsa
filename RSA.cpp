#include <iostream>
#include <math.h> 
#include <chrono> 
#include "RSA.h"

int main(){
	uint64_t a0[] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0xFFFFBBBFFFFFFFFF,0x77940,0x0,0xF000000000000000,0xD000000000000000,0xF000000001000000};
	uint64_t b0[] = {0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0xACE,0x0,0x0,0x0,0x0,0x0FFFFFFFFFFFFFFF,0xE000000000000000,0xF000000000000000};
	uint64_t c0[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x000000000000000A,0x1010101010101010};
	uint64_t d0[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0x000000000000000B,0x0101010101010101};
	bit2048 a(a0);
	bit2048 b(b0);
	bit2048 c(c0);
	bit2048 d(d0);
	
	auto start = std::chrono::steady_clock::now();
	
	cout<<a<<"\n+\n"<<b<<"\n=\n";
	cout<<(a+b)<<"\n\n";
	cout<<a<<"\n*\n"<<c<<"\n=\n";
	cout<<(a*c)<<"\n\n";
	cout<<a<<"\n*\n"<<b<<"\n=\n";
	cout<<(a*b)<<"\n\n";
	cout<<c<<"\n*\n"<<d<<"\n=\n";
	cout<<(c*d)<<"\n\n";
	cout<<d<<"\n-\n"<<c<<"\n=\n";
	cout<<(d-c)<<"\n\n";
	cout<<b<<"\n-\n"<<a<<"\n=\n";
	cout<<(b-a)<<"\n\n";
	cout<<a<<"\n>>200=\n";
	cout<<(a>>200)<<"\n\n";
	
	auto end = std::chrono::steady_clock::now();
	std::chrono::duration<double> elapsed_seconds = end-start;
    std::cout << "\nTemps d'exécution : " << 1000*elapsed_seconds.count() << "ms\n";
	return 0;}
	
uint64_t msb_mul(uint64_t a, uint64_t b){
	uint64_t a_lo = (uint32_t)a;
	uint64_t a_hi = a >> 32;
	uint64_t b_lo = (uint32_t)b;
	uint64_t b_hi = b >> 32;
	uint64_t a_x_b_hi =  a_hi * b_hi;
	uint64_t a_x_b_mid = a_hi * b_lo;
	uint64_t b_x_a_mid = b_hi * a_lo;
	uint64_t a_x_b_lo =  a_lo * b_lo;

	uint64_t carry_bit = ((uint64_t)(uint32_t)a_x_b_mid + (uint64_t)(uint32_t)b_x_a_mid + (a_x_b_lo >> 32) ) >> 32;
	uint64_t msb = a_x_b_hi + (a_x_b_mid >> 32) + (b_x_a_mid >> 32) + carry_bit;

return msb;
}

bit2048 montgomery(bit2048 A, bit2048 B, bit2048 N){
	uint64_t r0[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	bit2048 r(r0);
	int i = 0;
	while(N.data_get(i) == 0 )i++;
	if(N.data_get(i) >= 0x8000000000000000) r.data_set(i-1,1);
	else{
	r.data_set(i,1);
	while(r.data_get(i) < N.data_get(i)) r.data_set(i, r.data_get(i)<<1);
	}
	bit2048 S = A*B;
	
}